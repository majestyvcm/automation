package com.automation.sq.jenkins.report.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name="sumrepdetail_devs")
public class TbSumReportDeveloperDetail {
	
	@Id
	@Column(name="repdetail_id")
	private String repDetailId;
	
	@JsonIgnoreProperties("reportDetails")
	@ManyToOne
	@JoinColumn(name="rep_id")
	private TbSumReportConditionName repId;
	
	@Column(name="sonar_proj_name")
	private String sonarProjName;
	
	@Column(name="qt_status")
	private char qtStatus;
	
	@Column(name="repdate")
	private Date repDate;
	
	@Column(name="dev_name")
	private String devName;
	
	@Column(name="blocker")
	private int blocker;
	
	@Column(name="critical")
	private int critical;
	
	@Column(name="major")
	private int major;
	
	@Column(name="minor")
	private int minor;
	
	@Column(name="info")
	private int info;
	
	
	
	public String getRepDetailId() {
		return repDetailId;
	}
	public void setRepDetailId(String repDetailId) {
		this.repDetailId = repDetailId;
	}
	public TbSumReportConditionName getRepId() {
		return repId;
	}
	public void setRepId(TbSumReportConditionName repId) {
		this.repId = repId;
	}
	public String getSonarProjName() {
		return sonarProjName;
	}
	public void setSonarProjName(String sonarProjName) {
		this.sonarProjName = sonarProjName;
	}
	public char getQtStatus() {
		return qtStatus;
	}
	public void setQtStatus(char qtStatus) {
		this.qtStatus = qtStatus;
	}
	public Date getRepDate() {
		return repDate;
	}
	public void setRepDate(Date repDate) {
		this.repDate = repDate;
	}
	public String getDevName() {
		return devName;
	}
	public void setDevName(String devName) {
		this.devName = devName;
	}
	public int getBlocker() {
		return blocker;
	}
	public void setBlocker(int blocker) {
		this.blocker = blocker;
	}
	public int getCritical() {
		return critical;
	}
	public void setCritical(int critical) {
		this.critical = critical;
	}
	public int getMajor() {
		return major;
	}
	public void setMajor(int major) {
		this.major = major;
	}
	public int getMinor() {
		return minor;
	}
	public void setMinor(int minor) {
		this.minor = minor;
	}
	public int getInfo() {
		return info;
	}
	public void setInfo(int info) {
		this.info = info;
	}
	
	
}
