package com.automation.sq.jenkins.report.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

import com.automation.sq.jenkins.report.model.TbSumReportConditionName;
import com.automation.sq.jenkins.report.model.TbSumReportDeveloperDetail;
import com.automation.sq.jenkins.report.model.json.QtStatusComponents;
import com.automation.sq.jenkins.report.model.json.QtStatusModel;
import com.automation.sq.jenkins.report.model.json.SummaryDetailDevModel;

public class JSONModelToDBModelConverter {
	
	
	public static TbSumReportConditionName qtStatusJsonModelToDBModel(QtStatusModel qtStatus,
			List<QtStatusComponents> qtComponents) {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		Date d = new Date();
		TbSumReportConditionName sumrepObj = new TbSumReportConditionName();
		JSONModelToDBModelConverter converter = new JSONModelToDBModelConverter();
		try {
			sumrepObj.setRepId(sdf.format(d) + converter.generateRandomString());
			sumrepObj.setRepDate(d);
			sumrepObj.setSonarProjName(qtStatus.getProjectName());
			sumrepObj.setQtStatus(qtStatus.getProjectStatus().charAt(0));
			// Don't forget to change to obtain from list automatically
			sumrepObj.setReliabilityRate(qtComponents.get(0).getActualValue().charAt(0));
			sumrepObj.setSecurityRate(qtComponents.get(1).getActualValue().charAt(0));
			sumrepObj.setMaintainabilityRate(qtComponents.get(2).getActualValue().charAt(0));
			sumrepObj.setBlockerIssues(Integer.parseInt(qtComponents.get(3).getActualValue()));
			sumrepObj.setCriticalIssues(Integer.parseInt(qtComponents.get(4).getActualValue()));
			sumrepObj.setMajorIssues(Integer.parseInt(qtComponents.get(5).getActualValue()));
			sumrepObj.setDuplicateLines(Float.parseFloat(qtComponents.get(6).getActualValue().replace(" %", "")));
		} catch (Exception e) {
			System.err.println("ExtractSQClientController jsonModelToDBModel results error: " + e);
		}

		return sumrepObj;
	}
	
	public static List<TbSumReportDeveloperDetail> sumDevJsonModelToDBModel(List<SummaryDetailDevModel> qtComponents,
			TbSumReportConditionName qtGateStat) {
		List<TbSumReportDeveloperDetail> lr = new ArrayList<TbSumReportDeveloperDetail>();
		TbSumReportDeveloperDetail sumDev = new TbSumReportDeveloperDetail();
		SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy");
		Date d = new Date();
		JSONModelToDBModelConverter converter = new JSONModelToDBModelConverter();
		try {
			if (qtGateStat.getQtStatus() == 'F') {
				for (SummaryDetailDevModel obj : qtComponents) {
					sumDev = new TbSumReportDeveloperDetail();
					sumDev.setRepDate(d);
					sumDev.setSonarProjName(qtGateStat.getSonarProjName());
					sumDev.setQtStatus(qtGateStat.getQtStatus());
					sumDev.setRepId(qtGateStat);
					sumDev.setRepDetailId(sdf.format(d) + converter.generateRandomString());
					sumDev.setBlocker(Integer.parseInt(obj.getBlocker()));
					sumDev.setCritical(Integer.parseInt(obj.getCritical()));
					sumDev.setMajor(Integer.parseInt(obj.getMajor()));
					sumDev.setMinor(Integer.parseInt(obj.getMinor()));
					sumDev.setDevName(obj.getAuthor());
					sumDev.setInfo(Integer.parseInt(obj.getInfo()));
					lr.add(sumDev);
				}
			} else if (qtGateStat.getQtStatus() == 'P') {
				sumDev.setRepDate(d);
				sumDev.setSonarProjName(qtGateStat.getSonarProjName());
				sumDev.setQtStatus(qtGateStat.getQtStatus());
				sumDev.setRepId(qtGateStat);
				sumDev.setRepDetailId(sdf.format(d) + (int) (Math.random() * 500) + 1);
				lr.add(sumDev);
			}

		} catch (Exception e) {
			System.err.println("ExtractSQClientController jsonModelToDBModel results error: " + e);
		}

		return lr;
	}
	
	
	public String generateRandomString() {
		String string="abcdefghijklmnopqrstuvwxyzABCDEFGHIKLMNOPQRSTUVWXYZ0123456789";
		String randomString = null;
		Random rnd = new Random();
		try {
			randomString = "" + string.charAt(rnd.nextInt(string.length()))
					+ string.charAt(rnd.nextInt(string.length()))
					+ string.charAt(rnd.nextInt(string.length()));
		} catch (Exception e) {
			System.err.println("ExtractSQClientController generateRandomCharacter results error: " + e);
		}
		return randomString;
	}
}
