package com.automation.sq.jenkins.report.model.json;

import java.util.ArrayList;

public class QtStatusModel {
	private String projectStatus;
	private String projectName;
	ArrayList<QtStatusComponents> conditions = new ArrayList<QtStatusComponents>();

	public String getProjectStatus() {
		return projectStatus;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectStatus(String projectStatus) {
		this.projectStatus = projectStatus;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public ArrayList<QtStatusComponents> getConditions() {
		return conditions;
	}

	public void setConditions(ArrayList<QtStatusComponents> conditions) {
		this.conditions = conditions;
	}

}
