package com.automation.sq.jenkins.report.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;



import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.automation.sq.jenkins.report.model.TbBankNameBranch;
import com.automation.sq.jenkins.report.model.TbSumReportConditionName;
import com.automation.sq.jenkins.report.model.TbSumReportDeveloperDetail;
import com.automation.sq.jenkins.report.service.ExtractSQService;

@Controller
//@ResponseBody
public class ExtractSQController {

	@Autowired
	private ExtractSQService es;


	@GetMapping("/getAllReport")
	@ResponseBody
	public List<TbSumReportConditionName> getAllReport() {
		List<TbSumReportConditionName> lr = new ArrayList<TbSumReportConditionName>();
		try {
			lr = es.getAllReport();
		} catch (Exception e) {
			// TODO: handle exception
			System.err.println("ExtractSQController getAllReport: " + e);
		}
		return lr;
	}

	@PostMapping("/addSumReport")
	@ResponseBody
	public TbSumReportConditionName addNewSummaryReport(@RequestBody TbSumReportConditionName sumrep) {
		TbSumReportConditionName obj = new TbSumReportConditionName();
		try {
			obj = es.addReport(sumrep);
		} catch (Exception e) {
			// TODO: handle exception
			System.err.println("ExtractSQController addNewSummaryReport: " + e);
		}
		return obj;
	}

	@GetMapping("/getAllReportDetails") // , produces=MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public List<TbSumReportDeveloperDetail> getAllReportDetails() {
		List<TbSumReportDeveloperDetail> list = new ArrayList<TbSumReportDeveloperDetail>();
		try {
			list = es.getAllReportDetails();
		} catch (Exception e) {
			// TODO: handle exception
			System.err.println("ExtractSQController getAllReportDetails: " + e);
		}
		return list;
	}

	@GetMapping("/getReportDetailByRepId")
	@ResponseBody
	public List<TbSumReportDeveloperDetail> getReportDetailByRepId(@RequestParam String repId) {
		List<TbSumReportDeveloperDetail> list = new ArrayList<TbSumReportDeveloperDetail>();
		try {
			list = es.getReportDetailByRepId(repId);
		} catch (Exception e) {
			// TODO: handle exception
			System.err.println("ExtractSQController getReportDetailByRepId: " + e);
		}
		return list;
	}

	@PostMapping("/addRepDetailDev")
	@ResponseBody
	public TbSumReportDeveloperDetail addNewReportDetailDev(@RequestBody TbSumReportDeveloperDetail sumdev) {
		TbSumReportDeveloperDetail obj = new TbSumReportDeveloperDetail();
		try {
			obj = es.addRepDetailDev(sumdev);
		} catch (Exception e) {
			// TODO: handle exception
			System.err.println("ExtractSQController addRepDetailDev: " + e);
		}
		return obj;
	}

	@GetMapping("/getlistprojectkey")
	@ResponseBody
	public List<String> getListProjKey() {
		// TEST list
		List<String> list = new ArrayList<String>();
		try {
			list.add("com.aprisma.product:PCash5-Corporate-Mega-BackEnd");
			list.add("com.aprisma.product:PCash6-Corporate-Danamon-BackEnd");
			list.add("com.aprisma.product:PCash5-Corporate-CIMB-Niaga-BackEnd");
			list.add("com.aprisma.product:PCash5-Corporate-Mandiri-MCM-BackEnd");
			list.add("com.aprisma.product:PCash5-Corporate-Maybank-BackEnd");
			 
			
		} catch (Exception e) {
			// TODO: handle exception
			System.err.println("ExtractSQController getlistprojectkey: " + e);
		}
		return list;
	}

	@GetMapping("/showAllReport")
	public String displayAllReport(Model model) {
		List<TbSumReportConditionName> listRep = new ArrayList<TbSumReportConditionName>();
		List<TbSumReportDeveloperDetail> listDtlRep = new ArrayList<TbSumReportDeveloperDetail>();
		List<TbBankNameBranch> listRepBankBranch = new ArrayList<TbBankNameBranch>();

		try {
			listRep = es.getAllReport();
			model.addAttribute("listreport", listRep);
			for (TbSumReportConditionName rep : listRep) {
				listDtlRep = es.getReportDetailByRepId(rep.getRepId());
				rep.setReportDetails(listDtlRep);
				listRepBankBranch.add(es.getBySonarProjName(rep.getSonarProjName()));
			}
			model.addAttribute("listRepBankBranch", listRepBankBranch);
		} catch (Exception e) {
			// TODO: handle exception
			System.err.println("ExtractSQController showAllReport: " + e);
		}
		return "list-report";
	}

	@PostMapping(params = "print", value = "/AllSummaryReportAction")
	public String generateAllSummaryReportPage(ModelMap model) throws ParseException {

		XSSFWorkbook workbook = new XSSFWorkbook();
		XSSFSheet sheet = workbook.createSheet("Summary Report");

		List<TbSumReportConditionName> listRep = new ArrayList<TbSumReportConditionName>();
		TbBankNameBranch bank = new TbBankNameBranch();
		Map<String, Object[]> repData = new TreeMap<String, Object[]>();

		String[] headerRep = new String[] { "Project Name", "Branch", "Sonar Project Name", "Date Status",
				"Quality Status", "Reliability Rating\n on New Code", "Security Rating\n on New Code",
				"Maintainability Rating\n on New Code", "New Blocker\n Issues", "New Critical\n Issues",
				"New Major\n Issues", "Duplicated Lines\n on New Code(%)", "Total Issues" };

		int headerCol = 0;
		int rowNum = 0;
		boolean mergeOneRow = false;

		try {

			Row row = sheet.createRow(rowNum++);
			for (String title : headerRep) {
				sheet.autoSizeColumn(headerCol);
				Cell cell = row.createCell(headerCol++);
				cell.setCellValue(title);
				cell.setCellStyle(ExcelHeaderStyle(workbook));
			}

			listRep = es.getAllReport();
			for (TbSumReportConditionName rep : listRep) {
				bank = es.getBySonarProjName(rep.getSonarProjName());
				int totalIssueTemp = rep.getBlockerIssues() + rep.getCriticalIssues() + rep.getMajorIssues();
				String qtStat = (rep.getQtStatus() == 'F') ? "FAILED" : "PASSED";
				repData.put(rep.getRepId(),
						new Object[] { bank.getBankName(), bank.getProjBranch(), rep.getSonarProjName(),
								rep.getRepDate(), qtStat, rep.getReliabilityRate(), rep.getSecurityRate(),
								rep.getMaintainabilityRate(), rep.getBlockerIssues(), rep.getCriticalIssues(),
								rep.getMajorIssues(), rep.getDuplicateLines(), totalIssueTemp });
			}

			InsertExcelBody(repData, row, sheet, workbook, rowNum, mergeOneRow);

			File excel = new File("c:\\CI SDA\\Task\\Violation Report\\Extracted\\Summary Report.xlsx");
			FileOutputStream out = new FileOutputStream(excel);
			workbook.write(out);
			out.close();
			System.out.println("Summary Report.xlsx written successfully on disk.");

		} catch (Exception e) {
			System.err.println("ExtractSQController generateAllSummaryReportPage results error: ");
			e.printStackTrace();
		}

		return "SuccessGenerate";
	}

	@PostMapping(params = "merge", value = "/AllSummaryReportAction")
	public String mergeAllSummaryReport(ModelMap model) throws ParseException {

		List<TbSumReportConditionName> listRep = new ArrayList<TbSumReportConditionName>();
		TbBankNameBranch bank = new TbBankNameBranch();
		Map<String, Object[]> repData = new TreeMap<String, Object[]>();

		FileInputStream fileToMerge = null;
		FileOutputStream out = null;
		File excel = null;
		XSSFWorkbook workbook = null;
		XSSFSheet sheet = null;
		boolean mergeOneRow = false;

		try {
			fileToMerge = new FileInputStream(new File("c:\\CI SDA\\Task\\Violation Report\\Extracted\\Summary Report W42.xlsx"));
			
			workbook = new XSSFWorkbook(fileToMerge);
			sheet = workbook.getSheetAt(0);

			int rowNum = sheet.getLastRowNum();
			for (int i = sheet.getLastRowNum(); i > 0; i--) {
				Row row = sheet.getRow(i);
				if (row == null)
					row = sheet.createRow(i);
				Cell cell = row.getCell(0);
				if (cell == null) {
					cell = row.createCell(0);
					cell.setCellStyle(ExcelBodyStyle(workbook));
				}
				if (cell.toString() != "") {
					rowNum = ++i;
					break;
				}

			}

			Row row = null;

			listRep = es.getAllReport();
			for (TbSumReportConditionName rep : listRep) {
				bank = es.getBySonarProjName(rep.getSonarProjName());
				int totalIssueTemp = rep.getBlockerIssues() + rep.getCriticalIssues() + rep.getMajorIssues();
				String qtStat = (rep.getQtStatus() == 'F') ? "FAILED" : "PASSED";
				repData.put(rep.getRepId(),
						new Object[] { bank.getBankName(), bank.getProjBranch(), rep.getSonarProjName(),
								rep.getRepDate(), qtStat, rep.getReliabilityRate(), rep.getSecurityRate(),
								rep.getMaintainabilityRate(), rep.getBlockerIssues(), rep.getCriticalIssues(),
								rep.getMajorIssues(), rep.getDuplicateLines(), totalIssueTemp });
			}

			InsertExcelBody(repData, row, sheet, workbook, rowNum, mergeOneRow);

			excel = new File("c:\\CI SDA\\Task\\Violation Report\\Extracted\\Summary Report W42.xlsx");
			out = new FileOutputStream(excel);
			workbook.write(out);
			out.close();
			fileToMerge.close();

		} catch (FileNotFoundException e) {
			System.err.println("ExtractSQController mergeAllSummaryReportPage cannot find file: ");
			e.printStackTrace();
		} catch (IOException e) {
			System.err.println("ExtractSQController mergeAllSummaryReportPage InputOutputStream error: ");
		} catch (Exception e) {
			System.err.println("ExtractSQController mergeAllSummaryReportPage results error: ");
			e.printStackTrace();
		} finally {
			System.out.println("File successfully merged.");
		}

		return "SuccessGenerate";
	}

	@GetMapping("/showAllReportDetailDev")
	public String displayAllReportDetailDev(Model model) {
		List<TbSumReportDeveloperDetail> listDtlRep = new ArrayList<TbSumReportDeveloperDetail>();
		List<TbBankNameBranch> listDetRepBank = new ArrayList<TbBankNameBranch>();

		try {
			listDtlRep = es.getAllReportDetails();
			for (TbSumReportDeveloperDetail dtlRep : listDtlRep) {
				listDetRepBank.add(es.getBySonarProjName(dtlRep.getSonarProjName()));
			}
			model.addAttribute("listDetailReportDev", listDtlRep);
			model.addAttribute("listDetailReportBank", listDetRepBank);

		} catch (Exception e) {
			System.err.println("ExtractSQController displayAllReportDetailDev results error: ");
			e.printStackTrace();
		}

		return "list-report-detail-developer";
	}

	@PostMapping(params = "print", value = "/AllReportDetailDevAction")
	public String generateAllReportDetailDev(Model model) {
		XSSFWorkbook workbook = new XSSFWorkbook();
		XSSFSheet sheet = workbook.createSheet("Developer Violation Report");

		List<TbSumReportDeveloperDetail> listDtlRep = new ArrayList<TbSumReportDeveloperDetail>();
		TbBankNameBranch bank = new TbBankNameBranch();
		Map<String, Object[]> repDtlData = new TreeMap<String, Object[]>();

		String[] headerRep = new String[] { "Project Name", "Branch", "Date Status", "Quality Status", "Developer",
				"Blocker", "Critical", "Major", "Minor", "Info" };

		int headerCol = 0;
		int rowNum = 0;
		boolean mergeOneRow = true;

		try {

			Row row = sheet.createRow(rowNum++);
			for (String title : headerRep) {
				sheet.autoSizeColumn(headerCol);
				Cell cell = row.createCell(headerCol++);
				cell.setCellValue(title);
				cell.setCellStyle(ExcelHeaderStyle(workbook));
			}

			listDtlRep = es.getAllReportDetails();
			for (TbSumReportDeveloperDetail rep : listDtlRep) {
				bank = es.getBySonarProjName(rep.getSonarProjName());
				String qtStat = (rep.getQtStatus() == 'F') ? "FAILED" : "PASSED";
				repDtlData.put(rep.getRepDetailId(),
						new Object[] { bank.getBankName(), bank.getProjBranch(), rep.getRepDate(), qtStat,
								rep.getDevName(), rep.getBlocker(), rep.getCritical(), rep.getMajor(), rep.getMinor(),
								rep.getInfo() });
			}

			InsertExcelBody(repDtlData, row, sheet, workbook, rowNum, mergeOneRow);

			File excel = new File("c:\\CI SDA\\Task\\Violation Report\\Extracted\\Developer Violation Report.xlsx");
			FileOutputStream out = new FileOutputStream(excel);
			workbook.write(out);
			out.close();
			System.out.println("Developer Violation Report.xlsx written successfully on disk.");

		} catch (Exception e) {
			System.err.println("ExtractSQController generateAllSummaryReportPage results error: ");
			e.printStackTrace();
		}

		return "SuccessGenerate";
	}

	@PostMapping(params = "merge", value = "/AllReportDetailDevAction")
	public String mergeAllReportDetailDev() throws ParseException {

		List<TbSumReportDeveloperDetail> listDtlRep = new ArrayList<TbSumReportDeveloperDetail>();
		TbBankNameBranch bank = new TbBankNameBranch();
		Map<String, Object[]> repDtlData = new TreeMap<String, Object[]>();

		FileInputStream fileToMerge = null;
		FileOutputStream out = null;
		File excel = null;
		XSSFWorkbook workbook = null;
		XSSFSheet sheet = null;
		boolean mergeOneRow = true;

		try {

			fileToMerge = new FileInputStream(
					new File("c:\\CI SDA\\Task\\Violation Report\\Extracted\\Developer Violation Report W42.xlsx"));
			workbook = new XSSFWorkbook(fileToMerge);
			sheet = workbook.getSheetAt(0);

			int rowNum = sheet.getLastRowNum();
			for (int i = sheet.getLastRowNum(); i > 0; i--) {
				Row row = sheet.getRow(i);
				if (row == null)
					row = sheet.createRow(i);
				Cell cell = row.getCell(3);
				if (cell == null) {
					cell = row.createCell(0);
					cell.setCellStyle(ExcelBodyStyle(workbook));
				}
				if (cell.toString() != "") {
					rowNum = ++i;
					break;
				}

			}

			Row row = null;

			listDtlRep = es.getAllReportDetails();
			for (TbSumReportDeveloperDetail rep : listDtlRep) {
				bank = es.getBySonarProjName(rep.getSonarProjName());
				String qtStat = (rep.getQtStatus() == 'F') ? "FAILED" : "PASSED";
				repDtlData.put(rep.getRepDetailId(),
						new Object[] { bank.getBankName(), bank.getProjBranch(), rep.getRepDate(), //
								qtStat, rep.getDevName(), rep.getBlocker(), rep.getCritical(), rep.getMajor(),
								rep.getMinor(), rep.getInfo() });
			}

			if ((sheet.getMergedRegion(sheet.getNumMergedRegions() - 1)) == null)
				sheet.addMergedRegion(new CellRangeAddress(rowNum, rowNum, 0, repDtlData.size()));

			InsertExcelBody(repDtlData, row, sheet, workbook, ++rowNum, mergeOneRow);

			excel = new File("c:\\CI SDA\\Task\\Violation Report\\Extracted\\Developer Violation Report W42.xlsx");
			out = new FileOutputStream(excel);
			workbook.write(out);
			out.close();
			fileToMerge.close();

		} catch (FileNotFoundException e) {
			System.err.println("ExtractSQController mergeAllReportDetailDev cannot find file: ");
			e.printStackTrace();
		} catch (IOException e) {
			System.err.println("ExtractSQController mergeAllReportDetailDev InputOutputStream error: ");
		} catch (Exception e) {
			System.err.println("ExtractSQController mergeAllReportDetailDev results error: ");
			e.printStackTrace();
		} finally {
			System.out.println("File successfully merged.");
		}

		return "SuccessGenerate";
	}

	
	
	
	public String mergeQualityGateReport(List<TbSumReportConditionName> listRep) {
		
		TbBankNameBranch bank = new TbBankNameBranch();
		Map<String, Object[]> repData = new TreeMap<String, Object[]>();

		FileInputStream fileToMerge = null;
		FileOutputStream out = null;
		File excel = null;
		XSSFWorkbook workbook = null;
		XSSFSheet sheet = null;
		boolean mergeOneRow = false;

		try {

			fileToMerge = new FileInputStream(
					new File("c:\\CI SDA\\Task\\Violation Report\\Extracted\\Summary Report W42.xlsx"));
			workbook = new XSSFWorkbook(fileToMerge);
			sheet = workbook.getSheetAt(0);

			int rowNum = sheet.getLastRowNum();
			for (int i = sheet.getLastRowNum(); i > 0; i--) {
				Row row = sheet.getRow(i);
				if (row == null)
					row = sheet.createRow(i);
				Cell cell = row.getCell(0);
				if (cell == null) {
					cell = row.createCell(0);
					cell.setCellStyle(ExcelBodyStyle(workbook));
				}
				if (cell.toString() != "") {
					rowNum = ++i;
					break;
				}

			}

			Row row = null;

			for (TbSumReportConditionName rep : listRep) {
				bank = new TbBankNameBranch();
				bank = es.getBySonarProjName(rep.getSonarProjName());
				
				int totalIssueTemp = rep.getBlockerIssues() + rep.getCriticalIssues() + rep.getMajorIssues();
				String qtStat = (rep.getQtStatus() == 'F') ? "FAILED" : "PASSED";
				repData.put(rep.getRepId(),
						new Object[] { bank.getBankName(), bank.getProjBranch(), rep.getSonarProjName(),
								rep.getRepDate(), qtStat, rep.getReliabilityRate(), rep.getSecurityRate(),
								rep.getMaintainabilityRate(), rep.getBlockerIssues(), rep.getCriticalIssues(),
								rep.getMajorIssues(), rep.getDuplicateLines(), totalIssueTemp });
			}

			InsertExcelBody(repData, row, sheet, workbook, rowNum, mergeOneRow);

			excel = new File("c:\\CI SDA\\Task\\Violation Report\\Extracted\\Summary Report W42.xlsx");
			out = new FileOutputStream(excel);
			workbook.write(out);
			out.close();
			fileToMerge.close();

		} catch (FileNotFoundException e) {
			System.err.println("ExtractSQController mergeQualityGateReport cannot find file: ");
			e.printStackTrace();
		} catch (IOException e) {
			System.err.println("ExtractSQController mergeQualityGateReport InputOutputStream error: ");
		} catch (Exception e) {
			System.err.println("ExtractSQController mergeQualityGateReport results error: ");
			e.printStackTrace();
		} finally {
			System.out.println("File successfully merged.");
		}

		return "SuccessGenerate";
	}
	
	
	
	private CellStyle ExcelHeaderStyle(XSSFWorkbook workbook) {
		CellStyle style = (CellStyle) workbook.createCellStyle();
		Font font = workbook.createFont();
		try {

			font.setColor(IndexedColors.WHITE.getIndex());
			style.setFont(font);
			style.setVerticalAlignment(VerticalAlignment.CENTER);
			style.setFillForegroundColor(IndexedColors.ROYAL_BLUE.getIndex());
			style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
			style.setWrapText(true);
			style.setAlignment(HorizontalAlignment.CENTER);
			style.setBorderBottom(BorderStyle.THIN);
			style.setBorderTop(BorderStyle.THIN);
			style.setBorderLeft(BorderStyle.THIN);
			style.setBorderRight(BorderStyle.THIN);

		} catch (Exception e) {
			System.err.println("ExtractSQController SetHeaderStyle results error: ");
			e.printStackTrace();
		}
		return style;
	}

	private CellStyle ExcelBodyStyle(XSSFWorkbook workbook) {
		CellStyle style = (CellStyle) workbook.createCellStyle();
		try {

			style.setWrapText(true);
			style.setAlignment(HorizontalAlignment.CENTER);
			style.setBorderBottom(BorderStyle.THIN);
			style.setBorderTop(BorderStyle.THIN);
			style.setBorderLeft(BorderStyle.THIN);
			style.setBorderRight(BorderStyle.THIN);

		} catch (Exception e) {
			System.err.println("ExtractSQController SetHeaderStyle results error: ");
			e.printStackTrace();
		}
		return style;
	}

	private void InsertExcelBody(Map<String, Object[]> map, Row row, XSSFSheet sheet, XSSFWorkbook workbook,
			Integer rowNum, boolean mergeOneRow) {

		SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
		Set<String> keyset = map.keySet();
		try {

			for (String key : keyset) {

				Object[] objArr = map.get(key);
				int cellnum = 0;

				row = sheet.createRow(rowNum++);
				for (Object obj : objArr) {
					sheet.autoSizeColumn(cellnum);
					Cell cell = row.createCell(cellnum++);

					if (obj instanceof String)
						cell.setCellValue((String) obj);
					else if (obj instanceof Integer)
						cell.setCellValue((Integer) obj);
					else if (obj instanceof Date)
						cell.setCellValue(sdf.format((Date) obj));
					else if (obj instanceof Character)
						cell.setCellValue((String.valueOf(obj)));
					else if (obj instanceof Float) {
						cell.setCellValue((Float) obj);
						// style.setDataFormat(workbook.createDataFormat().getFormat("00.%"));
					}

					cell.setCellStyle(ExcelBodyStyle(workbook));
				}

				if (mergeOneRow) {
					sheet.addMergedRegion(new CellRangeAddress(rowNum, rowNum, 0, objArr.length - 1));
					rowNum++;
				}
			}

		} catch (Exception e) {
			System.err.println("ExtractSQController InsertExcelBody results error: ");
			e.printStackTrace();
		}
	}


	@PostMapping("/addRepDetailDevList")
	@ResponseBody
	public List<TbSumReportDeveloperDetail> addNewReportDetailDevList(@RequestBody List<TbSumReportDeveloperDetail> sumdev) {
		List<TbSumReportDeveloperDetail> list = new ArrayList<TbSumReportDeveloperDetail>();
		try {
			for (TbSumReportDeveloperDetail r : sumdev) {
				list.add(es.addRepDetailDev(r));
			}

		} catch (Exception e) {
			// TODO: handle exception
			System.err.println("ExtractSQController addRepDetailDev: " + e);
		}
		return list;
	}

}
