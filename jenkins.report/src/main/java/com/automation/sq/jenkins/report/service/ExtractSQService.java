package com.automation.sq.jenkins.report.service;

import java.util.List;

import com.automation.sq.jenkins.report.model.TbBankNameBranch;
import com.automation.sq.jenkins.report.model.TbSumReportConditionName;
import com.automation.sq.jenkins.report.model.TbSumReportDeveloperDetail;

public interface ExtractSQService {
	
	public List<TbSumReportConditionName> getAllReport();
	public TbSumReportConditionName addReport(TbSumReportConditionName srcn);
	public TbSumReportConditionName getByRepId(String rid);
	
	public TbSumReportDeveloperDetail addRepDetailDev(TbSumReportDeveloperDetail srdd);
	public List<TbSumReportDeveloperDetail> getAllReportDetails();
	public List<TbSumReportDeveloperDetail> getReportDetailByRepId(String repId);
	
	public TbBankNameBranch getBySonarProjName(String spn);
}
