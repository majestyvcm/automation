package com.automation.sq.jenkins.report.model.json;

public class SummaryDetailDevModel {
	private String blocker;
	private String critical;
	private String major;
	private String minor;
	private String author;
	private String info;
	
	
	public String getBlocker() {
		return blocker;
	}
	public void setBlocker(String blocker) {
		this.blocker = blocker;
	}
	public String getCritical() {
		return critical;
	}
	public void setCritical(String critical) {
		this.critical = critical;
	}
	public String getMajor() {
		return major;
	}
	public void setMajor(String major) {
		this.major = major;
	}
	public String getMinor() {
		return minor;
	}
	public void setMinor(String minor) {
		this.minor = minor;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getInfo() {
		return info;
	}
	public void setInfo(String info) {
		this.info = info;
	}
	
	
}
