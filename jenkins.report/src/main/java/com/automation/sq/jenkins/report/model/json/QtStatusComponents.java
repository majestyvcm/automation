package com.automation.sq.jenkins.report.model.json;


public class QtStatusComponents {
	
	Object component = new Object();
	Object metrics = new Object();
	private String bestValue;
	private String actualValue;
	private String comparator;
	
	
	public Object getComponent() {
		return component;
	}
	public void setComponent(Object component) {
		this.component = component;
	}
	public Object getMetrics() {
		return metrics;
	}
	public void setMetrics(Object metrics) {
		this.metrics = metrics;
	}

	public String getBestValue() {
		return bestValue;
	}
	public void setBestValue(String bestValue) {
		this.bestValue = bestValue;
	}
	public String getActualValue() {
		return actualValue;
	}
	public void setActualValue(String actualValue) {
		this.actualValue = actualValue;
	}
	public String getComparator() {
		return comparator;
	}
	public void setComparator(String comparator) {
		this.comparator = comparator;
	}
	
	
	
	
	
}
