package com.automation.sq.jenkins.report.model.json;

import java.util.ArrayList;

public class JSONSonarComponentModel {
	Object paging;
	ArrayList<Object> organizations = new ArrayList<Object>();
	ArrayList<JSONSonarComponentProjectKey> components = new ArrayList<JSONSonarComponentProjectKey>();
	ArrayList<Object> facets = new ArrayList<Object>();

	public Object getPaging() {
		return paging;
	}

	public void setPaging(Object paging) {
		this.paging = paging;
	}

	public ArrayList<Object> getOrganizations() {
		return organizations;
	}

	public void setOrganizations(ArrayList<Object> organizations) {
		this.organizations = organizations;
	}

	public ArrayList<JSONSonarComponentProjectKey> getComponents() {
		return components;
	}

	public void setComponents(ArrayList<JSONSonarComponentProjectKey> components) {
		this.components = components;
	}

	public ArrayList<Object> getFacets() {
		return facets;
	}

	public void setFacets(ArrayList<Object> facets) {
		this.facets = facets;
	}

}
