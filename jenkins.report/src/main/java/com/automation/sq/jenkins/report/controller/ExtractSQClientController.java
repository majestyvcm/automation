package com.automation.sq.jenkins.report.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.cert.X509Certificate;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;

import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.TrustStrategy;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.simple.JSONObject;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.automation.sq.jenkins.report.model.TbBankNameBranch;
import com.automation.sq.jenkins.report.model.TbSumReportConditionName;
import com.automation.sq.jenkins.report.model.TbSumReportDeveloperDetail;
import com.automation.sq.jenkins.report.model.json.JSONSonarComponentModel;
import com.automation.sq.jenkins.report.model.json.JSONSonarComponentProjectKey;
import com.automation.sq.jenkins.report.model.json.QtStatusModel;
import com.automation.sq.jenkins.report.model.json.SummaryDetailDevModel;
import com.automation.sq.jenkins.report.service.ExtractSQService;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

@Controller
public class ExtractSQClientController {

	@Autowired
	private ExtractSQService es;

	@Autowired
	private RestTemplate restTemplate;

	private String accessToken;

	private String getAccessToken() {
		return accessToken;
	}

	private void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	@PostMapping(value = "/generateAccessToken")
	@ResponseBody
	public String generateAcessToken() {
		JSONObject jsonObject = new JSONObject();
		String str = "{\n" + "	\"username\":\"ci.sda.tu\",\n" + "	\"password\":\"@Wirecard2019\"\n" + "}";
		String t = null;
		HttpEntity<String> entity = null;
		HttpHeaders headers = null;
		try {
			headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			entity = new HttpEntity<>(str, headers);
			jsonObject = restTemplate.exchange("http://10.10.230.142:4200/dashboard-idm/authenticate/login",
					HttpMethod.POST, entity, JSONObject.class).getBody();
			t = (String) jsonObject.get("access_token");
			this.setAccessToken(t);
			System.out.println(jsonObject);
		} catch (Exception e) {
			System.err.println("ExtractSQClientController generateAccessToken results error: " + e);
		}
		return t;
	}

	public String checkOrGetAccessToken(String accessToken) {
		HttpEntity<String> getTokenEntity = null;
		HttpHeaders headers = new HttpHeaders();
		
		try {
			if (accessToken == null) {
				headers.setContentType(MediaType.APPLICATION_JSON);
				getTokenEntity = new HttpEntity<String>(headers);
				accessToken = restTemplate.exchange(
						"http://localhost:8080/generateAccessToken", HttpMethod.POST,
						getTokenEntity, String.class).getBody();
				return accessToken;
			} else {

				
				/*
				 * response = restTemplate.exchange(
				 * "http://10.10.230.142:4200/dashboard-idm/ops-services/issue/getQualityGateDetails/"
				 * + "com.aprisma.product:PCash5-Corporate-Permata-BackEnd" + "/",
				 * HttpMethod.GET, null, String.class);
				 * 
				 * if (response.getStatusCode() == HttpStatus.UNAUTHORIZED) {
				 * headers.setContentType(MediaType.APPLICATION_JSON); getTokenEntity = new
				 * HttpEntity<String>(headers); accessToken =
				 * restTemplate.exchange("http://localhost:8080/generateAccessToken",
				 * HttpMethod.POST, getTokenEntity, String.class).getBody(); } return
				 * accessToken;
				 */
				
				
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return accessToken;
	}

	@RequestMapping(value = "/getQualityGateStatus", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public TbSumReportConditionName getQualityGateStatus(@RequestParam String projectKey) {
		HttpHeaders headers = new HttpHeaders();
		ResponseEntity<String> response = null;
		HttpEntity<String> getQtStatusEntity = null;

		String bearerToken = null;
		ObjectMapper mapper = new ObjectMapper();

		QtStatusModel qtStatusObj = new QtStatusModel();
		TbSumReportConditionName sumRep = new TbSumReportConditionName();

		try {
			headers.setContentType(MediaType.APPLICATION_JSON);
			bearerToken = checkOrGetAccessToken(getAccessToken());
			headers.setBearerAuth(bearerToken);
			getQtStatusEntity = new HttpEntity<String>(headers);

			response = restTemplate
					.exchange("http://10.10.230.142:4200/dashboard-idm/ops-services/issue/getQualityGateDetails/"
							+ projectKey + "/", HttpMethod.GET, getQtStatusEntity, String.class);

			qtStatusObj = mapper.readValue(response.getBody(), QtStatusModel.class);
			sumRep = JSONModelToDBModelConverter.qtStatusJsonModelToDBModel(qtStatusObj, qtStatusObj.getConditions());

			System.out.println("Get quality gate status " + qtStatusObj.getProjectName());

		} catch (Exception e) {
			System.err.println("ExtractSQClientController getQualityGateStatus results error: " + e);
		}
		return sumRep;
	}

	@RequestMapping(value = "/getSummaryDetailDev", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public List<SummaryDetailDevModel> getSummaryDetailDev(@RequestParam String projectKey) {
		List<SummaryDetailDevModel> sumDev = new ArrayList<SummaryDetailDevModel>();

		HttpHeaders headers = new HttpHeaders();
		ResponseEntity<String> response = null;
		HttpEntity<String> getSummaryDevEntity = null;

		String bearerToken = null;
		ObjectMapper mapper = new ObjectMapper();

		try {
			headers.setContentType(MediaType.APPLICATION_JSON);
			bearerToken = checkOrGetAccessToken(getAccessToken());

			headers.setBearerAuth(bearerToken);
			headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
			getSummaryDevEntity = new HttpEntity<String>(headers);

			response = restTemplate.exchange(
					"http://10.10.230.142:4200/dashboard-idm/ops-services/issue/getSummaryDev/" + projectKey + "/",
					HttpMethod.GET, getSummaryDevEntity, String.class);
			sumDev = mapper.readValue(response.getBody(), new TypeReference<List<SummaryDetailDevModel>>() {});

			int i = 0;
			for (SummaryDetailDevModel obj : sumDev) {
				System.out.println("Get summary developer " + i + " " + obj.getAuthor());
				i++;
			}

		} catch (Exception e) {
			System.err.println("ExtractSQClientController getSummaryDetailDev results error ");
			e.printStackTrace();
		}
		return sumDev;
	}

	@RequestMapping(value = "/saveQtStatusReport", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public TbSumReportConditionName saveQtStatusReport(@RequestParam String projectKey) {
		TbSumReportConditionName sumrep = new TbSumReportConditionName();
		HttpEntity<String> getQtStatusEntity = null;
		try {
			UriComponentsBuilder uriBuilder = UriComponentsBuilder
					.fromHttpUrl("http://localhost:8080/getQualityGateStatus").queryParam("projectKey", projectKey);
			sumrep = (TbSumReportConditionName) restTemplate.postForObject(uriBuilder.toUriString(), getQtStatusEntity,
					TbSumReportConditionName.class);
			es.addReport(sumrep);
			System.out.println("Save Qt Gate Status " + sumrep.getSonarProjName());
		} catch (Exception e) {
			System.err.println("ExtractSQClientController saveQtStatusReport results error: " + e);
		}
		return sumrep;
	}

	@RequestMapping(value = "/saveSumDevReport", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public List<TbSumReportDeveloperDetail> saveSumDevReport(@RequestParam String projectKey, @RequestBody TbSumReportConditionName sumRep) {
		List<TbSumReportDeveloperDetail> sumDevList = new ArrayList<TbSumReportDeveloperDetail>();
		List<SummaryDetailDevModel> rawSumDevList = new ArrayList<SummaryDetailDevModel>();
		HttpEntity<String> getSumDevEntity = null;
		ResponseEntity<SummaryDetailDevModel[]> response = null;
		
		try {
			UriComponentsBuilder uriBuilder = UriComponentsBuilder
					.fromHttpUrl("http://localhost:8080/getSummaryDetailDev").queryParam("projectKey", projectKey);

			response = restTemplate.exchange(uriBuilder.toUriString(), HttpMethod.GET, getSumDevEntity,
					SummaryDetailDevModel[].class);
			rawSumDevList = Arrays.asList(response.getBody());
			sumDevList = JSONModelToDBModelConverter.sumDevJsonModelToDBModel(rawSumDevList, sumRep);

			int i = 0;
			for (TbSumReportDeveloperDetail rep : sumDevList) {
				System.out.println("Save Summary Developer " + i + " " + rep.getDevName());
				es.addRepDetailDev(rep);
				i++;

			}

		} catch (Exception e) {
			System.err.println("ExtractSQClientController saveSumDevReport results error ");
			e.printStackTrace();
		}
		return sumDevList;
	}

	@RequestMapping(value = "/Synchronize", method = { RequestMethod.GET, RequestMethod.POST })
	@ResponseBody
	public List<TbSumReportConditionName> synchronize() {
		List<TbSumReportDeveloperDetail> ld = new ArrayList<TbSumReportDeveloperDetail>();
		List<TbSumReportConditionName> listReport = new ArrayList<TbSumReportConditionName>();
		TbSumReportConditionName sumRep = new TbSumReportConditionName();
		HttpEntity<String> entity = null;
		UriComponentsBuilder uriBuilder = null;
		UriComponentsBuilder uriBuilder2 = null;
		List<String> listProjectKey = new ArrayList<String>();
		try {
			
			listProjectKey = Arrays.asList(restTemplate.exchange("http://localhost:8080/testScrape", HttpMethod.GET, null, String[].class).getBody());
			
			
			for (String projKey : listProjectKey) {
				uriBuilder = UriComponentsBuilder.fromHttpUrl("http://localhost:8080/saveQtStatusReport")
						.queryParam("projectKey", projKey);

				sumRep = (TbSumReportConditionName) restTemplate.postForObject(uriBuilder.toUriString(), entity,
						TbSumReportConditionName.class);

				uriBuilder2 = UriComponentsBuilder.fromHttpUrl("http://localhost:8080/saveSumDevReport")
						.queryParam("projectKey", projKey);

				ld = Arrays.asList(restTemplate.postForObject(uriBuilder2.toUriString(), sumRep,
						TbSumReportDeveloperDetail[].class));
				
				sumRep.setReportDetails(ld);
				listReport.add(sumRep);
			}
		} catch (Exception e) {
			System.err.println("ExtractSQClientController /testSync results error: ");
			e.printStackTrace();
		}
		return listReport;

	}
	
	@Scheduled(cron = "0 53 16 * * ?") // Everyday 16:53:00
	@RequestMapping(value = "/SynchronizeAction", params="syncAndMerge", method = {RequestMethod.GET, RequestMethod.POST})
	public String synchronizeAndMerge() {
		List<TbSumReportConditionName> listReport = new ArrayList<TbSumReportConditionName>();
		HttpEntity<String> entity = null;
		ResponseEntity<String> response = null;
		ObjectMapper mapper = new ObjectMapper();
		try {
			response = restTemplate.exchange("http://localhost:8080/Synchronize", HttpMethod.GET, entity, String.class);
			listReport = mapper.readValue(response.getBody(), new TypeReference<List<TbSumReportConditionName>>() {});
			mergeQualityGateReport(listReport);
			mergeSummaryDevReport(listReport);
			
		} catch (Exception e) {
			System.err.println("ExtractSQClientController syncAndMerge results error: ");
			e.printStackTrace();
		}
		
		return "SuccessGenerate";
	}

	
	public void mergeQualityGateReport(List<TbSumReportConditionName> listRep) {
		TbBankNameBranch bank = new TbBankNameBranch();
		Map<String, Object[]> repData = new TreeMap<String, Object[]>();

		FileInputStream fileToMerge = null;
		FileOutputStream out = null;
		File excel = null;
		File sourceFile = null;
		XSSFWorkbook workbook = null;
		XSSFSheet sheet = null;
		boolean mergeOneRow = false;
		String fileDest = "c:\\CI SDA\\Task\\Violation Report\\Extracted\\Summary Report W4.xlsx";

		try {
			sourceFile = new File(fileDest);
			if (sourceFile.createNewFile()) {
				generateAllSummaryReport(listRep, fileDest);//
			} else {
				fileToMerge = new FileInputStream(sourceFile);
				workbook = new XSSFWorkbook(fileToMerge);
				sheet = workbook.getSheetAt(0);

				int rowNum = sheet.getLastRowNum();
				for (int i = sheet.getLastRowNum(); i > 0; i--) {
					Row row = sheet.getRow(i);
					if (row == null)
						row = sheet.createRow(i);
					Cell cell = row.getCell(0);
					if (cell == null) {
						cell = row.createCell(0);
						cell.setCellStyle(ExcelExtractProperties.ExcelBodyStyle(workbook));
					}
					if (cell.toString() != "") {
						rowNum = ++i;
						break;
					}

				}

				Row row = null;

				for (TbSumReportConditionName rep : listRep) {
					bank = new TbBankNameBranch();
					bank = es.getBySonarProjName(rep.getSonarProjName());

					int totalIssueTemp = rep.getBlockerIssues() + rep.getCriticalIssues() + rep.getMajorIssues();
					String qtStat = (rep.getQtStatus() == 'F') ? "FAILED" : "PASSED";
					repData.put(rep.getRepId(),
							new Object[] { bank.getBankName(), bank.getProjBranch(), rep.getSonarProjName(),
									rep.getRepDate(), qtStat, rep.getReliabilityRate(), rep.getSecurityRate(),
									rep.getMaintainabilityRate(), rep.getBlockerIssues(), rep.getCriticalIssues(),
									rep.getMajorIssues(), rep.getDuplicateLines(), totalIssueTemp });
				}

				ExcelExtractProperties.InsertExcelBody(repData, row, sheet, workbook, rowNum, mergeOneRow);

				//excel = new File("c:\\CI SDA\\Task\\Violation Report\\Extracted\\Summary Report W42.xlsx");
				out = new FileOutputStream(sourceFile);
				workbook.write(out);
				out.close();
				fileToMerge.close();
			}
		} catch (FileNotFoundException e) {
			System.err.println("ExtractSQController mergeQualityGateReport cannot find file: ");
			e.printStackTrace();
		} catch (IOException e) {
			System.err.println("ExtractSQController mergeQualityGateReport InputOutputStream error: ");
		} catch (Exception e) {
			System.err.println("ExtractSQController mergeQualityGateReport results error: ");
			e.printStackTrace();
		} finally {
			System.out.println("File successfully merged.");
		}

		
	}

	
	public void mergeSummaryDevReport(List<TbSumReportConditionName> listRep) {
		List<TbSumReportDeveloperDetail> listDtlRep = new ArrayList<TbSumReportDeveloperDetail>();
		TbBankNameBranch bank = new TbBankNameBranch();
		Map<String, Object[]> repDtlData = new TreeMap<String, Object[]>();

		FileInputStream fileToMerge = null;
		FileOutputStream out = null;
		File sourceFile = null;
		File excel = null;
		XSSFWorkbook workbook = null;
		XSSFSheet sheet = null;
		boolean mergeOneRow = true;
		String fileDest = "c:\\CI SDA\\Task\\Violation Report\\Extracted\\Summary Report W4.xlsx";

		try {
			sourceFile = new File(fileDest);
			if (sourceFile.createNewFile()) {
				for(TbSumReportConditionName qtGateRep : listRep) {
					listDtlRep.addAll(qtGateRep.getReportDetails());
				}
				generateAllReportDetailDev(listDtlRep, fileDest);
			}
			fileToMerge = new FileInputStream(sourceFile);
			workbook = new XSSFWorkbook(fileToMerge);
			sheet = workbook.getSheetAt(0);

			int rowNum = sheet.getLastRowNum();
			for (int i = sheet.getLastRowNum(); i > 0; i--) {
				Row row = sheet.getRow(i);
				if (row == null)
					row = sheet.createRow(i);
				Cell cell = row.getCell(3);
				if (cell == null) {
					cell = row.createCell(0);
					cell.setCellStyle(ExcelExtractProperties.ExcelBodyStyle(workbook));
				}
				if (cell.toString() != "") {
					rowNum = ++i;
					break;
				}

			}

			Row row = null;

			for(TbSumReportConditionName qtGateRep : listRep) {
				listDtlRep.addAll(qtGateRep.getReportDetails());
			}
				
			//listDtlRep = es.getAllReportDetails();
			for (TbSumReportDeveloperDetail rep : listDtlRep) {
				bank = es.getBySonarProjName(rep.getSonarProjName());
				String qtStat = (rep.getQtStatus() == 'F') ? "FAILED" : "PASSED";
				repDtlData.put(rep.getRepDetailId(),
						new Object[] { bank.getBankName(), bank.getProjBranch(), rep.getRepDate(),
								qtStat, rep.getDevName(), rep.getBlocker(), rep.getCritical(), rep.getMajor(),
								rep.getMinor(), rep.getInfo() });
			}

			if ((sheet.getMergedRegion(sheet.getNumMergedRegions() - 1)) == null)
				sheet.addMergedRegion(new CellRangeAddress(rowNum, rowNum, 0, repDtlData.size()));

			ExcelExtractProperties.InsertExcelBody(repDtlData, row, sheet, workbook, ++rowNum, mergeOneRow);

			excel = new File("c:\\CI SDA\\Task\\Violation Report\\Extracted\\Developer Violation Report W42.xlsx");
			out = new FileOutputStream(excel);
			workbook.write(out);
			out.close();
			fileToMerge.close();
			
			System.out.println("File successfully merged.");
			
		} catch (FileNotFoundException e) {
			System.err.println("ExtractSQController mergeSummaryDevReport cannot find file: ");
			e.printStackTrace();
		} catch (IOException e) {
			System.err.println("ExtractSQController mergeSummaryDevReport InputOutputStream error: ");
		} catch (Exception e) {
			System.err.println("ExtractSQController mergeSummaryDevReport results error: ");
			e.printStackTrace();
		} 

	}
	
	
	public void generateAllSummaryReport(List<TbSumReportConditionName> listRep, String fileDest) throws ParseException {

		XSSFWorkbook workbook = new XSSFWorkbook();
		XSSFSheet sheet = workbook.createSheet("Summary Report");

		//List<TbSumReportConditionName> listRep = new ArrayList<TbSumReportConditionName>();
		TbBankNameBranch bank = new TbBankNameBranch();
		Map<String, Object[]> repData = new TreeMap<String, Object[]>();

		String[] headerRep = new String[] { "Project Name", "Branch", "Sonar Project Name", "Date Status",
				"Quality Status", "Reliability Rating\n on New Code", "Security Rating\n on New Code",
				"Maintainability Rating\n on New Code", "New Blocker\n Issues", "New Critical\n Issues",
				"New Major\n Issues", "Duplicated Lines\n on New Code(%)", "Total Issues" };

		int headerCol = 0;
		int rowNum = 0;
		boolean mergeOneRow = false;

		try {

			Row row = sheet.createRow(rowNum++);
			for (String title : headerRep) {
				sheet.autoSizeColumn(headerCol);
				Cell cell = row.createCell(headerCol++);
				cell.setCellValue(title);
				cell.setCellStyle(ExcelExtractProperties.ExcelHeaderStyle(workbook));
			}

			listRep = es.getAllReport();
			for (TbSumReportConditionName rep : listRep) {
				bank = es.getBySonarProjName(rep.getSonarProjName());
				int totalIssueTemp = rep.getBlockerIssues() + rep.getCriticalIssues() + rep.getMajorIssues();
				String qtStat = (rep.getQtStatus() == 'F') ? "FAILED" : "PASSED";
				repData.put(rep.getRepId(),
						new Object[] { bank.getBankName(), bank.getProjBranch(), rep.getSonarProjName(),
								rep.getRepDate(), qtStat, rep.getReliabilityRate(), rep.getSecurityRate(),
								rep.getMaintainabilityRate(), rep.getBlockerIssues(), rep.getCriticalIssues(),
								rep.getMajorIssues(), rep.getDuplicateLines(), totalIssueTemp });
			}

			ExcelExtractProperties.InsertExcelBody(repData, row, sheet, workbook, rowNum, mergeOneRow);

			File excel = new File(fileDest);
			FileOutputStream out = new FileOutputStream(excel);
			workbook.write(out);
			out.close();
			System.out.println("Summary Report.xlsx written successfully on disk.");

		} catch (Exception e) {
			System.err.println("ExtractSQController generateAllSummaryReportPage results error: ");
			e.printStackTrace();
		}

		//return "SuccessGenerate";
	}
	
	
	public void generateAllReportDetailDev(List<TbSumReportDeveloperDetail> listDtlRep, String fileDest) {
		XSSFWorkbook workbook = new XSSFWorkbook();
		XSSFSheet sheet = workbook.createSheet("Developer Violation Report");

		//List<TbSumReportDeveloperDetail> listDtlRep = new ArrayList<TbSumReportDeveloperDetail>();
		TbBankNameBranch bank = new TbBankNameBranch();
		Map<String, Object[]> repDtlData = new TreeMap<String, Object[]>();

		String[] headerRep = new String[] { "Project Name", "Branch", "Date Status", "Quality Status", "Developer",
				"Blocker", "Critical", "Major", "Minor", "Info" };

		int headerCol = 0;
		int rowNum = 0;
		boolean mergeOneRow = true;

		try {

			Row row = sheet.createRow(rowNum++);
			for (String title : headerRep) {
				sheet.autoSizeColumn(headerCol);
				Cell cell = row.createCell(headerCol++);
				cell.setCellValue(title);
				cell.setCellStyle(ExcelExtractProperties.ExcelHeaderStyle(workbook));
			}

			listDtlRep = es.getAllReportDetails();
			for (TbSumReportDeveloperDetail rep : listDtlRep) {
				bank = es.getBySonarProjName(rep.getSonarProjName());
				String qtStat = (rep.getQtStatus() == 'F') ? "FAILED" : "PASSED";
				repDtlData.put(rep.getRepDetailId(),
						new Object[] { bank.getBankName(), bank.getProjBranch(), rep.getRepDate(), qtStat,
								rep.getDevName(), rep.getBlocker(), rep.getCritical(), rep.getMajor(), rep.getMinor(),
								rep.getInfo() });
			}

			ExcelExtractProperties.InsertExcelBody(repDtlData, row, sheet, workbook, rowNum, mergeOneRow);

			File excel = new File(fileDest);
			FileOutputStream out = new FileOutputStream(excel);
			workbook.write(out);
			out.close();
			System.out.println("Developer Violation Report.xlsx written successfully on disk.");

		} catch (Exception e) {
			System.err.println("ExtractSQController generateAllSummaryReportPage results error: ");
			e.printStackTrace();
		}

		//return "SuccessGenerate";
	}
	
	
	@RequestMapping(value = "/testScrape", method = {RequestMethod.GET, RequestMethod.POST})
	@ResponseBody
	public List<String> testScrape() {
		String url = "https://sonar.aprdev.com/api/components/search_projects";
		//?ps=50&facets=reliability_rating%2Csecurity_rating%2Csqale_rating%2Ccoverage%2Cduplicated_lines_density%2Cncloc%2Calert_status%2Clanguages%2Ctags&f=analysisDate%2CleakPeriodDate&s=analysisDate&asc=false
		HttpEntity<String> entity = null;
		ResponseEntity<String> response = null;
		HttpHeaders header = new HttpHeaders();
		List<String> list = new ArrayList<String>();
		try {
			//To disable SSL?
			TrustStrategy acceptingTrustStrategy = (X509Certificate[] chain, String authType) -> true;
			 
			SSLContext sslContext = org.apache.http.ssl.SSLContexts.custom()
			        .loadTrustMaterial(null, acceptingTrustStrategy)
			        .build();
			 
			SSLConnectionSocketFactory csf = new SSLConnectionSocketFactory(sslContext);
			 
			CloseableHttpClient httpClient = HttpClients.custom()
			        .setSSLSocketFactory(csf)
			        .build();
			 
			HttpComponentsClientHttpRequestFactory requestFactory =
			        new HttpComponentsClientHttpRequestFactory();
			 
			requestFactory.setHttpClient(httpClient);
			 
		 
			header.setContentType(MediaType.APPLICATION_JSON);
			header.set("Cookie", getCookieString());//"XSRF-TOKEN=74pgdtr478g38f562jdo6f8t2o; JWT-SESSION=eyJhbGciOiJIUzI1NiJ9.eyJqdGkiOiJBVzVwRzlYUGVzLVdWNkNSTGxvTSIsInN1YiI6Im1hamVzdHkubWFsdWVuc2VuZyIsImlhdCI6MTU3MzcyMTQ2MiwiZXhwIjoxNTczOTgwNjYyLCJsYXN0UmVmcmVzaFRpbWUiOjE1NzM3MjE0NjIyMjMsInhzcmZUb2tlbiI6Ijc0cGdkdHI0NzhnMzhmNTYyamRvNmY4dDJvIn0.RR0ipGOJdDDwGU_cYE20H8sfiRHYJRVQsh9mdMkrfvo");

			UriComponentsBuilder uriBuilder = UriComponentsBuilder.fromHttpUrl(url).queryParam("ps", 50)
					.queryParam("f", "analysisDate")
					.queryParam("s", "analysisDate")
					.queryParam("asc", false);
			entity = new HttpEntity<String>(header);

			response = new RestTemplate(requestFactory).exchange(uriBuilder.toUriString(), HttpMethod.GET, entity, String.class);

			System.out.println(response.getBody());
			list = getListProjectKey(response.getBody());
			//testScrapyy();
		} catch (Exception e) {
			System.err.println("ExtractSQClientController testScrape results error: ");
			e.printStackTrace();
		}
		return list;
	}
	
	
	public String getCookieString () {
		String cookiesString = null;
		try {
			TrustStrategy acceptingTrustStrategy = (X509Certificate[] chain, String authType) -> true;

			SSLContext sslContext = org.apache.http.ssl.SSLContexts.custom()
					.loadTrustMaterial(null, acceptingTrustStrategy).build();
			HttpsURLConnection.setDefaultSSLSocketFactory(sslContext.getSocketFactory());

			Connection.Response loginForm = Jsoup.connect("https://sonar.aprdev.com/sessions/new")
					.method(Connection.Method.GET)
					.userAgent(
							"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.97 Safari/537.36")
					.execute();
	
			
			Connection.Response document = Jsoup.connect("https://sonar.aprdev.com/api/authentication/login")
		  
		            .data("login", "majesty.maluenseng")
		            .data("password", "PO_kdvWN8cX3")
		            .cookies(loginForm.cookies())
		            .followRedirects(true)
		            .method(Connection.Method.POST).execute();
			Map<String, String> cookies = new HashMap<String, String>();
			cookies = document.cookies();
			System.out.println(cookies);
			cookiesString = cookies.toString().replace("{", "").replace("}", "").replace(",", ";");
		} catch (Exception e) {
			System.err.println("ExtractSQClientController testScrapyy results error: ");
			e.printStackTrace();
		}
		return cookiesString;
	}
	
	
	
	public List<String> getListProjectKey(String jsonString) {
		Date analysisDate = null;
		//Date today = new Date();
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");//'T'HH:mm:ss
		String today = sdf.format(new Date());
		JSONSonarComponentModel object = new JSONSonarComponentModel();
		ObjectMapper mapper = new ObjectMapper();
		List<String> list = new ArrayList<String>();
		try {
			
			object = mapper.readValue(jsonString, JSONSonarComponentModel.class);
			mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
			
			for(JSONSonarComponentProjectKey proj:object.getComponents()) {
				analysisDate = sdf.parse(proj.getAnalysisDate());
				if (analysisDate.equals(sdf.parse(today))) { 
					list.add(proj.getKey());
				}
				
			}
			
			
		} catch (Exception e) {
			System.err.println("ExtractSQClientController syncAndMerge results error: ");
			e.printStackTrace();
		}

		return list;

	}
	
	
	
	
	
}
