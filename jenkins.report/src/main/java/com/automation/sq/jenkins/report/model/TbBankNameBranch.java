package com.automation.sq.jenkins.report.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="bank_name_branch")
public class TbBankNameBranch {

	@Id
	@Column(name="sonar_proj_name")
	private String sonarProjName;
	
	@Column(name="bank_name")
	private String bankName;
	
	@Column(name="project_branch")
	private String projBranch;
	
	
	public String getSonarProjName() {
		return sonarProjName;
	}
	public void setSonarProjName(String sonarProjName) {
		this.sonarProjName = sonarProjName;
	}
	public String getBankName() {
		return bankName;
	}
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}
	public String getProjBranch() {
		return projBranch;
	}
	public void setProjBranch(String projBranch) {
		this.projBranch = projBranch;
	}
	
	
	
}
