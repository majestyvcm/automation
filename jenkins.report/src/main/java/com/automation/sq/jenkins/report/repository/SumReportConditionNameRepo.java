package com.automation.sq.jenkins.report.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.automation.sq.jenkins.report.model.TbSumReportConditionName;

@Repository
public interface SumReportConditionNameRepo extends JpaRepository<TbSumReportConditionName, String>{
	
	public TbSumReportConditionName findByRepId(String repId);
	
}
