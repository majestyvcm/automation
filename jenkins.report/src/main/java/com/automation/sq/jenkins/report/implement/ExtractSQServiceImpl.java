package com.automation.sq.jenkins.report.implement;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.automation.sq.jenkins.report.model.TbBankNameBranch;
import com.automation.sq.jenkins.report.model.TbSumReportConditionName;
import com.automation.sq.jenkins.report.model.TbSumReportDeveloperDetail;
import com.automation.sq.jenkins.report.repository.BankNameBranchRepo;
import com.automation.sq.jenkins.report.repository.SumReportConditionNameRepo;
import com.automation.sq.jenkins.report.repository.SumReportDeveloperDetailRepo;
import com.automation.sq.jenkins.report.service.ExtractSQService;

@Service
@Transactional
public class ExtractSQServiceImpl implements ExtractSQService {

	@Autowired
	private SumReportConditionNameRepo srcnpRepo;
	
	@Autowired
	private SumReportDeveloperDetailRepo srddrRepo;
	
	@Override
	public List<TbSumReportConditionName> getAllReport() {
		List<TbSumReportConditionName> lr = new ArrayList<TbSumReportConditionName>();
		try {
			lr = srcnpRepo.findAll();
		} catch (Exception e) {
			System.err.println("ExtractSQServiceImpl getAllReport results error: "+e);
		}
		return lr;
	}

	
	@Override
	public TbSumReportConditionName addReport(TbSumReportConditionName srcn) {
		TbSumReportConditionName obj = new TbSumReportConditionName();
		try {
			obj = srcnpRepo.save(srcn);
		} catch (Exception e) {
			System.err.println("ExtractSQServiceImpl addReport results error: "+e);
		}
		return obj;
	}
	
	
	@Override
	public TbSumReportConditionName getByRepId(String rid) {
		TbSumReportConditionName obj = new TbSumReportConditionName();
		try {
			obj = srcnpRepo.findByRepId(rid);
		} catch (Exception e) {
			System.err.println("ExtractSQServiceImpl getByRepId results error: "+e);
		}
		return obj;
	}

	
	
	@Override
	public TbSumReportDeveloperDetail addRepDetailDev(TbSumReportDeveloperDetail srdd) {
		TbSumReportDeveloperDetail obj = new TbSumReportDeveloperDetail();
		try {
			obj = srddrRepo.save(srdd);
		}catch (Exception e) {
			System.err.println("ExtractSQServiceImpl addNewRepDetailDev results error: "+e);
		}
		return obj;
	}
	

	@Override
	public List<TbSumReportDeveloperDetail> getAllReportDetails() {
		List<TbSumReportDeveloperDetail> list = new ArrayList<TbSumReportDeveloperDetail>();
		try {
			list = srddrRepo.findAll();
		} catch (Exception e) {
			System.err.println("ExtractSQServiceImpl getAllReportDetails results error: "+e);
		}
	
		return list;
	}


	@Override
	public List<TbSumReportDeveloperDetail> getReportDetailByRepId(String repId) {
		List<TbSumReportDeveloperDetail> list = new ArrayList<TbSumReportDeveloperDetail>();
		TbSumReportConditionName sumrep = getByRepId(repId);
		try {
			list = srddrRepo.findByRepId(sumrep);
		} catch (Exception e) {
			System.err.println("ExtractSQServiceImpl getReportDetailById results error: "+e);
		}
	
		return list;
	}

	
	
	

	@Autowired
	private BankNameBranchRepo bnbRepo;

	@Override
	public TbBankNameBranch getBySonarProjName(String spn) {
		TbBankNameBranch obj = new TbBankNameBranch();
		try {
			obj = bnbRepo.findBySonarProjName(spn);
		} catch (Exception e) {
			System.err.println("ExtractSQServiceImpl getBySonarProjName results error: "+e);
		}
		return obj;
	}
	
	
	
	

}
