package com.automation.sq.jenkins.report.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;
import java.util.Set;

import org.apache.poi.ss.usermodel.BorderStyle;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.VerticalAlignment;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelExtractProperties {
	
	
	public static CellStyle ExcelHeaderStyle(XSSFWorkbook workbook) {
		CellStyle style = (CellStyle) workbook.createCellStyle();
		Font font = workbook.createFont();
		try {

			font.setColor(IndexedColors.WHITE.getIndex());
			style.setFont(font);
			style.setVerticalAlignment(VerticalAlignment.CENTER);
			style.setFillForegroundColor(IndexedColors.ROYAL_BLUE.getIndex());
			style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
			style.setWrapText(true);
			style.setAlignment(HorizontalAlignment.CENTER);
			style.setBorderBottom(BorderStyle.THIN);
			style.setBorderTop(BorderStyle.THIN);
			style.setBorderLeft(BorderStyle.THIN);
			style.setBorderRight(BorderStyle.THIN);

		} catch (Exception e) {
			System.err.println("ExtractSQController SetHeaderStyle results error: ");
			e.printStackTrace();
		}
		return style;
	}

	public static CellStyle ExcelBodyStyle(XSSFWorkbook workbook) {
		CellStyle style = (CellStyle) workbook.createCellStyle();
		try {

			style.setWrapText(true);
			style.setAlignment(HorizontalAlignment.CENTER);
			style.setBorderBottom(BorderStyle.THIN);
			style.setBorderTop(BorderStyle.THIN);
			style.setBorderLeft(BorderStyle.THIN);
			style.setBorderRight(BorderStyle.THIN);

		} catch (Exception e) {
			System.err.println("ExtractSQController SetHeaderStyle results error: ");
			e.printStackTrace();
		}
		return style;
	}

	public static void InsertExcelBody(Map<String, Object[]> map, Row row, XSSFSheet sheet, XSSFWorkbook workbook, Integer rowNum, boolean mergeOneRow) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
		Set<String> keyset = map.keySet();
		try {

			for (String key : keyset) {

				Object[] objArr = map.get(key);
				int cellnum = 0;

				row = sheet.createRow(rowNum++);
				for (Object obj : objArr) {
					sheet.autoSizeColumn(cellnum);
					Cell cell = row.createCell(cellnum++);

					if (obj instanceof String)
						cell.setCellValue((String) obj);
					else if (obj instanceof Integer)
						cell.setCellValue((Integer) obj);
					else if (obj instanceof Date)
						cell.setCellValue(sdf.format((Date) obj));
					else if (obj instanceof Character)
						cell.setCellValue((String.valueOf(obj)));
					else if (obj instanceof Float) {
						cell.setCellValue((Float) obj);
						// style.setDataFormat(workbook.createDataFormat().getFormat("00.%"));
					}

					cell.setCellStyle(ExcelBodyStyle(workbook));
				}

				if (mergeOneRow) {
					sheet.addMergedRegion(new CellRangeAddress(rowNum, rowNum, 0, objArr.length - 1));
					rowNum++;
				}
			}

		} catch (Exception e) {
			System.err.println("ExtractSQController InsertExcelBody results error: ");
			e.printStackTrace();
		}
	}
	
	
}
