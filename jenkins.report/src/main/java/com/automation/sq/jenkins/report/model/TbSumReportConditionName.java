package com.automation.sq.jenkins.report.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name="sumrep_conditionname")
public class TbSumReportConditionName {
	
	@Id
	@Column(name="rep_id")
	private String repId;
	
	@Column(name="sonar_proj_name")
	private String sonarProjName;
	
	@Column(name="qt_status")
	private char qtStatus;
	
	@Column(name="rep_date")
	private Date repDate;
	
	@Column(name="reliability_rate")
	private char reliabilityRate;
	
	@Column(name="security_rate")
	private char securityRate;
	
	@Column(name="maintainability_rate")
	private char maintainabilityRate;
	
	@Column(name="blocker_issues")
	private int blockerIssues;
	
	@Column(name="critical_issues")
	private int criticalIssues;
	
	@Column(name="major_issues")
	private int majorIssues;
	
	@Column(name="duplicate_lines")
	private float duplicateLines;
	
	@JsonIgnoreProperties("repId")
	@OneToMany(mappedBy="repId")
	private List<TbSumReportDeveloperDetail> reportDetails;
	

	public String getRepId() {
		return repId;
	}

	public void setRepId(String repId) {
		this.repId = repId;
	}

	public String getSonarProjName() {
		return sonarProjName;
	}

	public void setSonarProjName(String sonarProjName) {
		this.sonarProjName = sonarProjName;
	}

	public char getQtStatus() {
		return qtStatus;
	}

	public void setQtStatus(char qtStatus) {
		this.qtStatus = qtStatus;
	}

	public Date getRepDate() {
		return repDate;
	}

	public void setRepDate(Date repDate) {
		this.repDate = repDate;
	}

	public char getReliabilityRate() {
		return reliabilityRate;
	}

	public void setReliabilityRate(char reliabilityRate) {
		this.reliabilityRate = reliabilityRate;
	}

	public char getSecurityRate() {
		return securityRate;
	}

	public void setSecurityRate(char securityRate) {
		this.securityRate = securityRate;
	}

	public char getMaintainabilityRate() {
		return maintainabilityRate;
	}

	public void setMaintainabilityRate(char maintainabilityRate) {
		this.maintainabilityRate = maintainabilityRate;
	}

	public int getBlockerIssues() {
		return blockerIssues;
	}

	public void setBlockerIssues(int blockerIssues) {
		this.blockerIssues = blockerIssues;
	}

	public int getCriticalIssues() {
		return criticalIssues;
	}

	public void setCriticalIssues(int criticalIssues) {
		this.criticalIssues = criticalIssues;
	}

	public int getMajorIssues() {
		return majorIssues;
	}

	public void setMajorIssues(int majorIssues) {
		this.majorIssues = majorIssues;
	}

	public float getDuplicateLines() {
		return duplicateLines;
	}

	public void setDuplicateLines(float duplicateLines) {
		this.duplicateLines = duplicateLines;
	}

	
	public List<TbSumReportDeveloperDetail> getReportDetails() {
		return reportDetails;
	}

	public void setReportDetails(List<TbSumReportDeveloperDetail> reportDetails) {
		this.reportDetails = reportDetails;
	}
	
	
	
}
