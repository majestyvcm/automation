package com.automation.sq.jenkins.report.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.automation.sq.jenkins.report.model.TbSumReportConditionName;
import com.automation.sq.jenkins.report.model.TbSumReportDeveloperDetail;

@Repository
public interface SumReportDeveloperDetailRepo extends JpaRepository<TbSumReportDeveloperDetail, String>{
	
	public TbSumReportDeveloperDetail findByRepDetailId(String repDetailId);
	public List<TbSumReportDeveloperDetail> findByRepId(TbSumReportConditionName repId);
	
}
