<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
<title>First Web Application</title>
</head>
<body>
   	<form method="post" action="/AllSummaryReportAction">
   		<table border="1">
   			<tr>
   				<th>Rep Id</th>
   				<th>Sonar Project Name</th>
   				<th>Quality Status</th>
   				<th>Rep Date</th>
   				<th>Reliability Rate</th>
   				<th>Security Rate</th>
   				<th>Maintainability Rate</th>
   				<th>Blocker Issues</th>
   				<th>Critical Issues</th>
   				<th>Major Issues</th>
   				<th>Duplicate Lines</th>
   			</tr>
   			<c:forEach var="rep" items="${listreport}" varStatus="loop">
   				<tr>
   					<td>${rep.repId}</td>
   					<td>${rep.sonarProjName}</td>
   					<td>${rep.qtStatus}</td>
   					<td><fmt:formatDate value="${rep.repDate}" pattern="yyyy-MM-dd"/></td>
   					<td>${rep.reliabilityRate}</td>
   					<td>${rep.securityRate}</td>
   					<td>${rep.maintainabilityRate}</td>
   					<td>${rep.blockerIssues}</td>
   					<td>${rep.criticalIssues}</td>
   					<td>${rep.majorIssues}</td>
   					<td>${rep.duplicateLines}</td>
   					<td>
   					<c:forEach var="repDtl" items="${rep.reportDetails}">
   							<li>${repDtl.repDetailId}|${repDtl.devName}|${repDtl.blocker}|
   								${repDtl.critical}|${repDtl.major}|${repDtl.minor}|${repDtl.info}</li>
   						</c:forEach>
   					</td>
   					<td>
   						${listRepBankBranch[loop.index].bankName}				
   					</td>
   				</tr>
   				
   			</c:forEach>
   		</table>
   		<input type="submit" value="Print Summary Report" name="print">
   		<input type="submit" value="Merge Summary Report" name="merge">
   	</form>
</body>
</html>