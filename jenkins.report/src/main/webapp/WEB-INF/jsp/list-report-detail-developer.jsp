<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<html>
<head>
<title>First Web Application</title>
</head>
<body>
   	<form method="post" action="/AllReportDetailDevAction">
   		<table border="1">
   			<tr>
   				<th>Detail Report Id</th>
   				<th>Bank Name</th>
   				<th>Sonar Project Name</th>
   				<th>Quality Status</th>
   				<th>Rep Date</th>
   				<th>Developer Name</th>
   				<th>Blocker</th>
   				<th>Critical</th>
   				<th>Major </th>
   				<th>Minor </th>
   				<th>Info</th>
   			</tr>
   			<c:forEach var="repDtl" items="${listDetailReportDev}" varStatus="loop">
   				<tr>
   					<td>${repDtl.repDetailId}</td>
   					<td>${listDetailReportBank[loop.index].bankName}</td>	
   					<td>${repDtl.sonarProjName}</td>
   					<td>${repDtl.qtStatus}</td>
   					<td><fmt:formatDate value="${repDtl.repDate}" pattern="dd-MM-yyyy"/></td>
   					<td>${repDtl.devName}</td>
   					<td>${repDtl.blocker}</td>
   					<td>${repDtl.critical}</td>
   					<td>${repDtl.major}</td>
   					<td>${repDtl.minor}</td>
   					<td>${repDtl.info}</td>
   					
   				</tr>
   				
   			</c:forEach>
   		</table>
   		<input type="submit" value="Print Developer Violation Report" name="print"/>
   		<input type="submit" value="Merge Developer Violation Report" name="merge"/>
   	</form>
</body>
</html>